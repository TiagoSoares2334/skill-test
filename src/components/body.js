import { useState } from "react"
import { useSelector, useDispatch } from "react-redux"
import { addItem } from "../redux/todoSlice"

import ListItem from "./listItems"

import "../styles/list.css"

const Body = () => {
    const [value, setValue] = useState("")
    const listItem = useSelector(state => state.items)
    const dispatch = useDispatch()

    const onSubmit = (e) => {
        e.preventDefault()
        dispatch(
            addItem({
                title: value,
            })
        )
        e.target.reset()
    }

    return ( 
        <div className="card">
            <form onSubmit={onSubmit} className="add-item">
                <input className="add-item-field"  type="text" onChange={(e) => setValue(e.target.value)} />
                <input type="submit" value="add" className="add-item-btn" />
            </form>

            <div className="card-body">
                {
                    listItem[0] ? listItem.map((item, index) => (
                        <ListItem item={item.title} id={item.id} completed={item.completed} key={index} /> 
                    ))
                    :
                    <div>Add your first todo item.</div>
                }
            </div>
        </div> 
    );
}
 
export default Body;