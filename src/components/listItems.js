import "../styles/list.css"
import { useDispatch } from "react-redux";
import { toggleCompleted, deleteItem } from "../redux/todoSlice";

const ListItem = (props) => {
    const dispatch = useDispatch()

    const handleComplete = () => {
        dispatch(
            toggleCompleted({
                id: props.id, 
                completed: !props.completed
            })
        )
    }

    const removeItem = () => {
        dispatch(
            deleteItem({
                id: props.id
            })
        )    
    }

    return ( 
        <div className={`li-container ${props.completed && 'li-completed'}`}>
            <input type="checkbox" checked={props.completed} onChange={handleComplete} />
            <div className="list">
                <div className="list-item">
                    <span className="li-title">{props.item}</span>
                </div>
                <button className="li-del">
                    <button onClick={removeItem}>X</button>
                </button>
            </div>
        </div> 
    );
}
 
export default ListItem;