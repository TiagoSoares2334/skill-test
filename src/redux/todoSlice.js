import { createSlice } from '@reduxjs/toolkit'

const itemsSlice = createSlice({
    name: "listItems",
    initialState: [
        
    ], 
    reducers: {
        addItem: (state, action) => {
            const newItem = {
                id: Date.now(), // only to get a unique value
                title: action.payload.title,
                completed: false
            }
            state.push(newItem)
        }, 
        toggleCompleted: (state, action) => {
            const index = state.findIndex( (item) => item.id === action.payload.id )
            state[index].completed = action.payload.completed
        }, 
        deleteItem: (state, action) => {
            return state.filter(item => item.id !== action.payload.id)
        }
    }
})

export const { addItem, toggleCompleted, deleteItem } = itemsSlice.actions;
export default itemsSlice.reducer;