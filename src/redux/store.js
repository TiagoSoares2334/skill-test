import { configureStore } from '@reduxjs/toolkit'
import itemsReducer from './todoSlice'

export default configureStore({ 
    reducer: {
        items: itemsReducer
    }
}); 